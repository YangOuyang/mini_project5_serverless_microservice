# mini_project5_serverless_microservice

## Initial Setups

### Step 1: Edit and test the basic function code locally 
1. Create a new directory for the project and change directory into it, then using `cargo init `
2. Adjust the `src/main.rs` file to contain the basic logic.

### Step 2: Create the aws lambda function
1. Using `Cargo lambda` to create the lambda function
2. Using `cargo lambda build` to build the lambda function
3. Using `cargo lambda deploy --region` to deploy the lambda function to AWS
2. Test the lambda function locally using `cargo lambda run`


### Step 3: Create the Serverless DynamoDB table
1. Go to the AWS console and search for DynamoDB
2. Click on create table
3. Enter the table name and the primary key `user`
4. Set the read and write capacity as per the requirements
4. Finished creating the table
5. ![create table](images/create_table.png)


## Connect the Lambda function to the DynamoDB table
1. Make sure the configuration of aws credentials is done in the local machine 
2. Write the code to use the aws_sdk_dynamodb library to interact with the DynamoDB table
3. Test the code locally again
4. Deploy the lambda function again using `cargo lambda deploy --region`
5. Configure the environment variable `TABLE_NAME` with the name of the DynamoDB table
6. Give the lambda function's role the permission to access the DynamoDB table
    ![alt text](images/dynamo_permission.png)
7. Test the lambda function in the AWS console
8. Here's the [demo](https://drive.google.com/file/d/10zqTEGzxESBt6UpPsVq5VVsLTr_EPApu/view?usp=drive_link)