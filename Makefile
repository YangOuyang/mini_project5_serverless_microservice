format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

run:
	cargo run

build:
	cargo lambda build --release

deploy:
	cargo lambda deploy --region us-east-1

all: format lint test run
